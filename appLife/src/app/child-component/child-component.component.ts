import { Component, OnInit, OnChanges, Input, SimpleChanges, DoCheck } from '@angular/core';

@Component({
  selector: 'app-child-component',
  templateUrl: './child-component.component.html',
  styleUrls: ['./child-component.component.css']
})
export class ChildComponentComponent implements OnInit, OnChanges, DoCheck {
  @Input() greeting: string;
  @Input() user: {name: string};

  oldUserName: string;
  changeDeected: boolean;
  changCount: 0;

  constructor() { }

  ngOnInit() {

    // let user: any = {
    //   name: 'zhangsan'
    // }
    // user.name = 'lisi'
    // 改变了对象里面的一个属性的指向，并不改变user对象的指针， 不可变元素 不会触发OnChanges钩子
  }

  ngOnChanges(OnChanges: SimpleChanges): void {
    console.log(JSON.stringify(OnChanges));
  }

  ngDoCheck(): void{
    if (this.user.name !== this.oldUserName ) {
      this.changeDeected = true;

      console.log("DoCheck ： user.name从" + this.oldUserName +  '变为' + this.user.name + this.changCount + '次' )
      this.oldUserName = this.user.name;
    } if ( this.changeDeected ) {
      this.changCount = 0;
    } else {
      this.changCount ++;
      console.log('没变化')
    }
  }
}
