import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CosultComponent } from './cosult.component';

describe('CosultComponent', () => {
  let component: CosultComponent;
  let fixture: ComponentFixture<CosultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CosultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CosultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
