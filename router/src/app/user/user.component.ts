import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from  '@angular/router';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.less']
})
export class UserComponent implements OnInit {

  constructor( private router: ActivatedRoute) { }
  private isMe : Boolean;

  ngOnInit() {
    this.isMe = this.router.snapshot.data[0].isMe;
  }

}
