import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { Page404Component } from './page404/page404.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { CosultComponent } from './cosult/cosult.component';
import { PermissionGuard } from './guard/permission.guard';
import { FocusGuard } from './guard/focus.guard';
const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/home'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'cosult',
    component: CosultComponent,
    outlet: 'aux'
  },
  {
    path: 'user',
    component: UserComponent,
    data: [{isMe: true}],
    children : [
      {
        path: 'login',
        component : LoginComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      }
    ],
    canActivate : [PermissionGuard ],
    canDeactivate: [FocusGuard],
  },
  {
    path: '**',
    component: Page404Component,
    // redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
