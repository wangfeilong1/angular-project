import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { Page404Component } from './page404/page404.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { CosultComponent } from './cosult/cosult.component';
import { PermissionGuard } from './guard/permission.guard';
import { FocusGuard } from './guard/focus.guard';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserComponent,
    Page404Component,
    LoginComponent,
    RegisterComponent,
    CosultComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [PermissionGuard, FocusGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
