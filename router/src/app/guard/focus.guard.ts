import { CanDeactivate } from '@angular/router';
import { UserComponent } from '../user/user.component';

export class FocusGuard implements CanDeactivate<UserComponent> {
  canDeactivate() {
    return false;
  }
}