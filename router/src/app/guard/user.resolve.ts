import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserComponent } from '../user/user.component';
import { Observable } from 'rxjs';

export class UserResolve implements Resolve<UserComponent> {
  constructor(private router: Router) {
    
  }
  resolve(router: ActivatedRouteSnapshot, state: RouterStateSnapshot,) : Observable<UserComponent> | Promise<UserComponent> {
    
  }
}