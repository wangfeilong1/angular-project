import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-course-search',
  templateUrl: './course-search.component.html',
  styleUrls: ['./course-search.component.less']
})
export class CourseSearchComponent implements OnInit {
  @Input()

  private keywords: string;
  private price: number
  constructor() { }

  ngOnInit() {
    setInterval (() =>{
      let courseInfo: CourseInfo = new CourseInfo ( this.keywords, 100 * Math.random())
      this.price = courseInfo.price;
    },3000)
    
  }
}

export class CourseInfo {
  constructor( public name: string, public price: number){}
}
